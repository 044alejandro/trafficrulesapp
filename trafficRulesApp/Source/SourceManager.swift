//
//  SourceManager.swift
//  trafficRulesApp
//
//  Created by Виктор Сирик on 24.10.2021.
//

import Foundation

class SourceManager {
    var offenders: [Offender] = []
    
    func getOffender(){
        offenders = setUpOffenders()
    }
    
    func addOffender(newOffender: Offender, arrayOffenders: [Offender]) -> [Offender] {
        var tempArray = arrayOffenders
        tempArray.append(newOffender)
        return tempArray.sorted(by: {$0.name < $1.name})
    }
    
    func setUpOffenders() -> [Offender] {
        let offender1 = Offender(name: "София", surname: "Анисимова", amountOfTheFine: "3400", offendersImageName: "sofiaImage", infoAboutFine: "Управление транспортным средством лицом, не имеющим права управления таким транспортным средством", offenderID: 1)
        let offender2 = Offender(name: "Василий", surname: "Александров", amountOfTheFine: "1700", offendersImageName: "vasyaImage", infoAboutFine: "Повторное на протяжении года нарушение ", offenderID: 2)
        let offender3 = Offender(name: "Марк", surname: "Савельев", amountOfTheFine: "510", offendersImageName: "markImage", infoAboutFine: "Нарушение водителями транспортных средств правил проезда перекрестков", offenderID: 3)
        let offender4 = Offender(name: "Иван", surname: "Лапшин", amountOfTheFine: "1445", offendersImageName: "ivanImage", infoAboutFine: "Нарушения, предусмотренные частями первой – четвертой данной статьи, которые вызвали создание аварийной обстановки, а именно: заставили других участников дорожного движения резко изменить скорость", offenderID: 4)
        let offender5 = Offender(name: "Мария", surname: "Ежова", amountOfTheFine: "340", offendersImageName: "mariaImage", infoAboutFine: "Превышение водителями транспортных средств установленных ограничений скорости движения более чем на двадцать километров в час", offenderID: 5)
        return [offender1, offender2, offender3, offender4, offender5]
    }
}
