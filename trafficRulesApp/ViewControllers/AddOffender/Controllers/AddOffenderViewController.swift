//
//  AddOffenderViewController.swift
//  trafficRulesApp
//
//  Created by Виктор Сирик on 24.10.2021.
//

import UIKit

class AddOffenderViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var surnameTextField: UITextField!
    @IBOutlet weak var infoAboutFineTextField: UITextField!
    @IBOutlet weak var amountOfTheFine: UITextField!
    
    var arrayOffenders: [Offender] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func addNewOffenderAction(_ sender: Any) {
        guard let name = nameTextField.text, let surname = surnameTextField.text, let sumOfTheFine = amountOfTheFine.text , let description = infoAboutFineTextField.text, !name.isEmpty, !surname.isEmpty, !sumOfTheFine.isEmpty, !description.isEmpty else {
            let alertVC = UIAlertController(title: "Error", message: "Enter fields", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            alertVC.addAction(cancelAction)
            present(alertVC, animated: true, completion: nil)
            return
        }
        
        let randomInt = Int.random(in: 6...100)
        let newOffender = Offender(name: name, surname: surname, amountOfTheFine: sumOfTheFine, offendersImageName: "default", infoAboutFine: description, offenderID: randomInt)
        let offendersVC = (self.tabBarController?.viewControllers?[0] as? ListOfOffendersViewController)
        arrayOffenders = SourceManager().addOffender(newOffender: newOffender, arrayOffenders: offendersVC?.offenders ?? [])
        offendersVC?.offenders = arrayOffenders
        self.tabBarController?.selectedIndex = 0
        nameTextField.text = ""
        surnameTextField.text = ""
        infoAboutFineTextField.text = ""
        amountOfTheFine.text = ""

    }
    
}
