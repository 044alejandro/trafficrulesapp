//
//  PaidOffFinesPeopleViewController + Extensions.swift
//  trafficRulesApp
//
//  Created by Виктор Сирик on 24.10.2021.
//

import UIKit

extension PaidOffFinesPeopleViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(paidOffFinesPeoples.count)
        return paidOffFinesPeoples.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: offendersCellID, for: indexPath) as! OffendersTableViewCell
        cell.update(offender: paidOffFinesPeoples[indexPath.row])
        return cell
    }
}
