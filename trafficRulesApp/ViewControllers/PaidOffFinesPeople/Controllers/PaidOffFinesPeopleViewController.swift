//
//  PaidOffFinesPeopleViewController.swift
//  trafficRulesApp
//
//  Created by Виктор Сирик on 24.10.2021.
//

import UIKit

class PaidOffFinesPeopleViewController: UIViewController {
    @IBOutlet weak var paidOffFinesPeopleTableView: UITableView!
    var paidOffFinesPeoples: [Offender] = []
    let offendersCellID = "OffendersTableViewCell"

    override func viewDidLoad() {
        super.viewDidLoad()

        paidOffFinesPeopleTableView.register(UINib(nibName: offendersCellID, bundle: nil), forCellReuseIdentifier: offendersCellID)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        paidOffFinesPeopleTableView.reloadData()
    }
    

}
