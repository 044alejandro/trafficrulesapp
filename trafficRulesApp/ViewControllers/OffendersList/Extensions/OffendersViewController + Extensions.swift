//
//  OffendersViewController + Extensions.swift
//  trafficRulesApp
//
//  Created by Виктор Сирик on 24.10.2021.
//

import UIKit

extension ListOfOffendersViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return offenders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: offendersCellID, for: indexPath) as! OffendersTableViewCell
        cell.update(offender: offenders[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            var paidOffFinesOffenders: [Offender] = []
            let paidOffFinesPeoplesVC = (self.tabBarController?.viewControllers?[2] as? PaidOffFinesPeopleViewController)
            
            paidOffFinesOffenders = paidOffFinesPeoplesVC?.paidOffFinesPeoples ?? []
            paidOffFinesOffenders.append(offenders[indexPath.row])
            offenders.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            paidOffFinesPeoplesVC?.paidOffFinesPeoples = paidOffFinesOffenders
        }
    }
}
