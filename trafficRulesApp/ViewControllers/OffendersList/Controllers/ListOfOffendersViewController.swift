//
//  ListOfOffendersViewController.swift
//  trafficRulesApp
//
//  Created by Виктор Сирик on 24.10.2021.
//

import UIKit

class ListOfOffendersViewController: UIViewController {
    @IBOutlet weak var offendersListTableView: UITableView!
    let offendersCellID = "OffendersTableViewCell"
    var offenders: [Offender] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        offendersListTableView.register(UINib(nibName: offendersCellID, bundle: nil), forCellReuseIdentifier: offendersCellID)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if offenders.isEmpty {
            offenders = SourceManager().setUpOffenders()
        }
        offendersListTableView.reloadData()
    }

}
