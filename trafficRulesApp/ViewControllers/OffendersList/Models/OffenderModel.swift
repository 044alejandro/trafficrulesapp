//
//  OffenderModel.swift
//  trafficRulesApp
//
//  Created by Виктор Сирик on 24.10.2021.
//

import Foundation

struct Offender {
    var name: String
    var surname: String
    var amountOfTheFine: String
    var offendersImageName: String
    var infoAboutFine: String
    var offenderID: Int
    
}
