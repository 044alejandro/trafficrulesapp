//
//  OffendersTableViewCell.swift
//  trafficRulesApp
//
//  Created by Виктор Сирик on 24.10.2021.
//

import UIKit

class OffendersTableViewCell: UITableViewCell {

    @IBOutlet weak var offendersNameSurnameLabel: UILabel!
    @IBOutlet weak var infoAboutFineLabel: UILabel!
    @IBOutlet weak var amountOfTheFineLabel: UILabel!
    @IBOutlet weak var offendersImage: UIImageView!
    
    func update(offender: Offender) {
        offendersNameSurnameLabel.text = "\(offender.name) \(offender.surname)"
        infoAboutFineLabel.text = offender.infoAboutFine
        amountOfTheFineLabel.text = "\(offender.amountOfTheFine) грн"
        offendersImage.image = UIImage(named: offender.offendersImageName)
    }
    
}
